﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pub/MasterPage.master" AutoEventWireup="true" CodeFile="ContactUs.aspx.cs" Inherits="Pub_ContactUs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpTitle" Runat="Server">
    Contact Us
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpHeader" Runat="Server">
    <link rel="stylesheet" type="text/css" href="../jobpro/css/responsive2.css" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cpContent" Runat="Server">
    <!-- jp Contact heading Wrapper Start -->
    <div class="jp_contact_heading_main_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="jp_contact_heading_wrapper">
                        <h2>Our Helpline 24x7 Available</h2>
                        <p>Call us at <span>+20 1210 3888 75</span> or Email us at <a href="#">support@ElGed3an.com</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="jp_contact_map_wrapper">
        <div id="map" style="width:100%; float:left; height:600px;"></div>
        <div class="jp_map_cont_label">
            <h3>You can Find us..</h3>
            <p>67 Al Gesr, Al Teraa Al Boulaqeyah, Shobra, Cairo Governorate, Egypt</p>
        </div>
    </div>
    <!-- jp Contact heading Wrapper End -->
    <!-- jp Contact form Wrapper Start -->
    <div class="jp_contact_form_main_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="jp_contact_form_heading_wrapper">
                        <h2>Leave A Message</h2>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="jp_contact_form_box">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div class="jp_contact_inputs_wrapper">
                                <i class="fa fa-user"></i><input type="text" placeholder="Name *">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div class="jp_contact_inputs_wrapper jp_contact_inputs2_wrapper">
                                <i class="fa fa-envelope"></i><input type="text" placeholder="Email *">
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_contact_inputs_wrapper jp_contact_inputs3_wrapper">
                                <i class="fa fa-pencil-square-o"></i><input type="text" placeholder="Subject">
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_contact_inputs_wrapper jp_contact_inputs4_wrapper">
                                <i class="fa fa-text-height"></i><textarea rows="6" placeholder="Type Your Message *"></textarea>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_contact_form_btn_wrapper">
                                <ul>
                                    <li><a href="#"><i class="fa fa-check"></i>&nbsp; SEND</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="jp_contact_right_box_wrapper">
                        <div class="jp_contact_form_add_heading_wrapper">
                            <h2>Contact Info</h2>
                        </div>
                        <div class="jp_form_add_wrapper gc_map_add_wrapper1">
                            <div class="jp_map_location_icon_wrapper">
                                <i class="fa fa-map-marker"></i>
                            </div>
                            <div class="gc_map_location_icon_cont_wrapper">
                                <h3>67 Al Gesr, Al Teraa Al Boulaqeyah, Shobra, Cairo Governorate, Egypt</h3>
                            </div>
                        </div>
                        <div class="jp_form_add_wrapper gc_map_add_wrapper2">
                            <div class="jp_map_location_icon_wrapper">
                                <i class="fa fa-phone"></i>
                            </div>
                            <div class="gc_map_location_icon_cont_wrapper gc_map_location_icon_cont_wrapper2">
                                <h3> +20 1210 3888 75</h3>
                            </div>
                        </div>
                        <div class="jp_form_add_wrapper gc_map_add_wrapper2">
                            <div class="jp_map_location_icon_wrapper">
                                <i class="fa fa-whatsapp"></i>
                            </div>
                            <div class="gc_map_location_icon_cont_wrapper gc_map_location_icon_cont_wrapper3">
                                <h3> +20 1010 57 1200</h3>
                            </div>
                        </div>
                        <div class="jp_form_add_wrapper gc_map_add_wrapper3">
                            <div class="jp_map_location_icon_wrapper">
                                <i class="fa fa-envelope"></i>
                            </div>
                            <div class="gc_map_location_icon_cont_wrapper gc_map_location_icon_cont_wrapper4">
                                <h3> <a href="#">support@ElGed3an.com</a></h3>
                            </div>
                        </div>
                        <div class="jp_contact_form_social_icon">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                <li class="hidden-xs"><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li class="hidden-xs"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- jp Contact form Wrapper End -->
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cpScript" Runat="Server">
    <script>
        function initMap() {
            var uluru = { lat: 30.0745793, lng: 31.251743999999995 };
        	var map = new google.maps.Map(document.getElementById('map'), {
        	zoom: 18,
        	scrollwheel: false,
        	center: uluru
        	});
        	var marker = new google.maps.Marker({
        	position: uluru,
        	map: map
        	});
        	}
    </script>
      <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCaESjfeNZMQw38WD9eFjYAftqmRFoE8Mg&callback=initMap&language=en&region=Eg" type="text/javascript"></script>
<%--    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCaESjfeNZMQw38WD9eFjYAftqmRFoE8Mg&amp;callback=initMap">
    </script>--%>
</asp:Content>

