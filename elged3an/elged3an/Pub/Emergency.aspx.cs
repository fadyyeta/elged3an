﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Pub_Emergency : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        lvEmergencies.DataSource = EmergencyClass.ShowAllEmergencies();
        lvEmergencies.DataBind();
    }
}