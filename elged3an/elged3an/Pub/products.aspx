﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pub/MasterPage.master" AutoEventWireup="true" CodeFile="products.aspx.cs" Inherits="Pub_products" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpTitle" Runat="Server">
    <asp:Literal Text="Products" ID="titleID" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpHeader" Runat="Server">
    <link rel="stylesheet" type="text/css" href="../App_Themes/products/assets/css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="../App_Themes/products/assets/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="../App_Themes/products/assets/vendor/owl-slider.css"/>
    <link rel="stylesheet" type="text/css" href="../App_Themes/products/assets/vendor/range-price.css"/>
    <link href="../App_Themes/products/assets/css/myProductsStyle.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cpContent" Runat="Server">
    <div class="listing-ver2">
          <div class="container">  
            <div id="Secondary" class="col-lg-3 col-md-3 col-sm-12 col-xs-12 hidden-sm hidden-xs sideBarLeft">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_rightside_job_categories_wrapper">
                                <div class="jp_rightside_job_categories_heading">
                                    <h4>Jobs by Category</h4>
                                </div>
                                <div class="jp_rightside_job_categories_content">
                                    <div class="handyman_sec1_wrapper">
                                        <div class="content">
                                            <div class="box">
                                                <p>
                                                    <input type="checkbox" id="c1" name="cb">
                                                    <label for="c1">Graphic Designer <span>(214)</span></label>

                                                    <p>
                                                        <input type="checkbox" id="c2" name="cb">
                                                        <label for="c2">Engineering Jobs <span>(514)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c3" name="cb">
                                                        <label for="c3">Mainframe Jobs <span>(554)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c4" name="cb">
                                                        <label for="c4">Legal Jobs <span>(457)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c5" name="cb">
                                                        <label for="c5">IT Jobs <span>(1254)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c6" name="cb">
                                                        <label for="c6">R&D Jobs <span>(554)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c7" name="cb">
                                                        <label for="c7">Government Jobs <span>(350)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c8" name="cb">
                                                        <label for="c8">PSU Jobs <span>(221)</span></label>
                                                    </p>
                                            </div>
                                        </div>
                                        <ul>
                                            <li><i class="fa fa-plus-circle"></i> <a href="#">SHOW MORE</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_rightside_job_categories_wrapper jp_job_location_wrapper">
                                <div class="jp_rightside_job_categories_heading">
                                    <h4>Jobs by Location</h4>
                                </div>
                                <div class="jp_rightside_job_categories_content">
                                    <div class="handyman_sec1_wrapper">
                                        <div class="content">
                                            <div class="box">
                                                <p>
                                                    <input type="checkbox" id="c9" name="cb">
                                                    <label for="c9">Jobs in Delhi  <span>(214)</span></label>

                                                    <p>
                                                        <input type="checkbox" id="c10" name="cb">
                                                        <label for="c10">Jobs in Mumbai <span>(514)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c11" name="cb">
                                                        <label for="c11">Jobs in Chennai <span>(554)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c12" name="cb">
                                                        <label for="c12">Jobs in Gurgaon <span>(457)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c13" name="cb">
                                                        <label for="c13">Jobs in Noida  <span>(1254)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c14" name="cb">
                                                        <label for="c14">Jobs in Kolkata <span>(554)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c15" name="cb">
                                                        <label for="c15">Jobs in Hyderabad <span>(350)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c16" name="cb">
                                                        <label for="c16">Jobs in Pune <span>(221)</span></label>
                                                    </p>
                                            </div>
                                        </div>
                                        <ul>
                                            <li><i class="fa fa-plus-circle"></i> <a href="#">SHOW MORE</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_rightside_job_categories_wrapper jp_job_location_wrapper">
                                <div class="jp_rightside_job_categories_heading">
                                    <h4>Your Skill’s</h4>
                                </div>
                                <div class="jp_rightside_job_categories_content">
                                    <div class="handyman_sec1_wrapper">
                                        <div class="content">
                                            <div class="box">
                                                <p>
                                                    <input type="checkbox" id="c17" name="cb">
                                                    <label for="c17">Javascript  <span>(214)</span></label>

                                                    <p>
                                                        <input type="checkbox" id="c18" name="cb">
                                                        <label for="c18">HTML5 <span>(514)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c19" name="cb">
                                                        <label for="c19">CSS3 <span>(554)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c20" name="cb">
                                                        <label for="c20">PHP  <span>(457)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c21" name="cb">
                                                        <label for="c21">Sales   <span>(1254)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c22" name="cb">
                                                        <label for="c22">Marketing  <span>(554)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c23" name="cb">
                                                        <label for="c23">Social Media <span>(350)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c24" name="cb">
                                                        <label for="c24">Web Design <span>(221)</span></label>
                                                    </p>
                                            </div>
                                        </div>
                                        <ul>
                                            <li><i class="fa fa-plus-circle"></i> <a href="#">SHOW MORE</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_rightside_job_categories_wrapper jp_job_location_wrapper">
                                <div class="jp_rightside_job_categories_heading">
                                    <h4>Salary</h4>
                                </div>
                                <div class="jp_rightside_job_categories_content">
                                    <div class="handyman_sec1_wrapper">
                                        <div class="content">
                                            <div class="box">
                                                <p>
                                                    <input type="checkbox" id="c25" name="cb">
                                                    <label for="c25">$1k - 2k  <span>(214)</span></label>

                                                    <p>
                                                        <input type="checkbox" id="c26" name="cb">
                                                        <label for="c26">$3k - 5k <span>(514)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c27" name="cb">
                                                        <label for="c27">$5k - 8k <span>(554)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c28" name="cb">
                                                        <label for="c28">$5k - 10k  <span>(457)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c29" name="cb">
                                                        <label for="c29">$10k - 20k   <span>(1254)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c30" name="cb">
                                                        <label for="c30">$20k - 30k  <span>(554)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c31" name="cb">
                                                        <label for="c31">$30k - 50k <span>(350)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c32" name="cb">
                                                        <label for="c32">$50k - 80k <span>(221)</span></label>
                                                    </p>
                                            </div>
                                        </div>
                                        <ul>
                                            <li><i class="fa fa-plus-circle"></i> <a href="#">SHOW MORE</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_rightside_job_categories_wrapper jp_job_location_wrapper">
                                <div class="jp_rightside_job_categories_heading">
                                    <h4>Types</h4>
                                </div>
                                <div class="jp_rightside_job_categories_content">
                                    <div class="handyman_sec1_wrapper">
                                        <div class="content">
                                            <div class="box">
                                                <p>
                                                    <input type="checkbox" id="c33" name="cb">
                                                    <label for="c33">Types  <span>(214)</span></label>

                                                    <p>
                                                        <input type="checkbox" id="c34" name="cb">
                                                        <label for="c34">Part-time <span>(514)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c35" name="cb">
                                                        <label for="c35">Contract <span>(554)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c36" name="cb">
                                                        <label for="c36">Remotely  <span>(457)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c37" name="cb">
                                                        <label for="c37">Temporary   <span>(1254)</span></label>
                                                    </p>
                                            </div>
                                        </div>
                                        <ul>
                                            <li><i class="fa fa-plus-circle"></i> <a href="#">SHOW MORE</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_add_resume_wrapper jp_job_location_wrapper">
                                <div class="jp_add_resume_img_overlay"></div>
                                <div class="jp_add_resume_cont">
                                    <img src="../jobpro/images/content/resume_logo.png" alt="logo" />
                                    <h4>Get Best Matched Jobs On your Email. Add Resume NOW!</h4>
                                    <ul>
                                        <li><a href="#"><i class="fa fa-plus-circle"></i> &nbsp;ADD RESUME</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- End Secondary -->
            <div id="primary" class="col-xs-12 col-md-9 sideBarRight">     
                    <!-- End Banner Grid -->
                    <div class="ordering">
                        <div class="ordering-left">
                            <span>Short by:</span>
                            <a href="#" title="NewestArrivals">NewestArrivals</a>
                            <a href="#" title="Price high to low">Price high to low</a>
                            <a href="#" title="Price low to high">Price low to high</a>
                            <a href="#" title="Most populer">Most populer</a>
                        </div>
                        <!-- end left -->
                        <div class="ordering-right">
                            <span class="list active"></span>
                            <span class="col"></span>
                            <form action="#" method="get" class="order-by">
                                <select class="orderby" name="orderby">
                                        <option>40 per page</option>
                                        <option>30 per page</option>
                                        <option>20 per page</option>
                                        <option>10 per page</option>
                                </select>
                            </form>
                        </div>
                        <!-- End right -->
                    </div>
                    <!-- End ordering -->
                    <div class="products grid_full grid_sidebar list-item">
                            <div class="item-inner">
                                <div class="product">
                                    <p class="product-title">Apple watch sport green</p>
                                    <p class="product-price"><span>price: </span>$ 650.99</p>
                                    <a class="product-images" href="#" title="">
                                        <img class="primary_image" src="../App_Themes/products/assets/images/products/1.jpg" alt="images">
                                        <img class="secondary_image" src="../App_Themes/products/assets/images/products/2.jpg" alt="images">
                                    </a>
                                    <p class="description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                                    <div class="action">
                                        <a class="refresh" href="#"><i class="zmdi zmdi-refresh-sync"></i></a>
                                        <a href="#" title="Like"><i class="zmdi zmdi-favorite-outline"></i></a>
                                        <a href="#" title="add-to-cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="item-inner">
                                <div class="product">
                                    <p class="product-title">Apple watch sport green</p>
                                    <p class="product-price"><span>price: </span>$ 650.99</p>
                                    <a class="product-images" href="#" title="">
                                        <img class="primary_image" src="../App_Themes/products/assets/images/products/3.jpg" alt="images">
                                        <img class="secondary_image" src="../App_Themes/products/assets/images/products/4.jpg" alt="images">
                                    </a>
                                    <p class="description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                                    <div class="action">
                                        <a class="refresh" href="#"><i class="zmdi zmdi-refresh-sync"></i></a>
                                        <a href="#" title="Like"><i class="zmdi zmdi-favorite-outline"></i></a>
                                        <a href="#" title="add-to-cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="item-inner">
                                <div class="product sale">
                                    <p class="product-title">Apple watch sport green</p>
                                    <div class="product-price">
                                        <span>price: </span><span class="price">$ 650.99</span>
                                        <span class="price-old">$ 670.99</span>
                                    </div>
                                    <a class="product-images" href="#" title="">
                                        <img class="primary_image" src="../App_Themes/products/assets/images/products/7.jpg" alt="images">
                                        <img class="secondary_image" src="../App_Themes/products/assets/images/products/6.jpg" alt="images">
                                    </a>
                                    <p class="description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                                    <div class="action">
                                        <a class="refresh" href="#"><i class="zmdi zmdi-refresh-sync"></i></a>
                                        <a href="#" title="Like"><i class="zmdi zmdi-favorite-outline"></i></a>
                                        <a href="#" title="add-to-cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="item-inner">
                                <div class="product ">
                                    <span class="new lable">New</span>
                                    <p class="product-title">Apple watch sport green</p>
                                    <p class="product-price"><span>price: </span>$ 650.99</p>
                                    <a class="product-images" href="#" title="">
                                        <img class="primary_image" src="../App_Themes/products/assets/images/products/2.jpg" alt="images">
                                        <img class="secondary_image" src="../App_Themes/products/assets/images/products/1.jpg" alt="images">
                                    </a>
                                    <p class="description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                                    <div class="action">
                                        <a class="refresh" href="#"><i class="zmdi zmdi-refresh-sync"></i></a>
                                        <a href="#" title="Like"><i class="zmdi zmdi-favorite-outline"></i></a>
                                        <a href="#" title="add-to-cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="item-inner">
                                <div class="product">
                                    <p class="product-title">Apple watch sport green</p>
                                    <p class="product-price"><span>price: </span>$ 650.99</p>
                                    <a class="product-images" href="#" title="">
                                        <img class="primary_image" src="../App_Themes/products/assets/images/products/4.jpg" alt="images">
                                        <img class="secondary_image" src="../App_Themes/products/assets/images/products/3.jpg" alt="images">
                                    </a>
                                    <p class="description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                                    <div class="action">
                                        <a class="refresh" href="#"><i class="zmdi zmdi-refresh-sync"></i></a>
                                        <a href="#" title="Like"><i class="zmdi zmdi-favorite-outline"></i></a>
                                        <a href="#" title="add-to-cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="item-inner">
                                <div class="product">
                                    <p class="product-title">Apple watch sport green</p>
                                    <p class="product-price"><span>price: </span>$ 650.99</p>
                                    <a class="product-images" href="#" title="">
                                        <img class="primary_image" src="../App_Themes/products/assets/images/products/6.jpg" alt="images">
                                        <img class="secondary_image" src="../App_Themes/products/assets/images/products/5.jpg" alt="images">
                                    </a>
                                    <p class="description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                                    <div class="action">
                                        <a class="refresh" href="#"><i class="zmdi zmdi-refresh-sync"></i></a>
                                        <a href="#" title="Like"><i class="zmdi zmdi-favorite-outline"></i></a>
                                        <a href="#" title="add-to-cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="item-inner">
                                <div class="product sale">
                                    <span class="sale lable">Sale</span>
                                    <p class="product-title">Apple watch sport green</p>
                                    <div class="product-price">
                                        <span>price: </span><span class="price">$ 650.99</span>
                                        <span class="price-old">$ 670.99</span>
                                    </div>
                                    <a class="product-images" href="#" title="">
                                        <img class="primary_image" src="../App_Themes/products/assets/images/products/7.jpg" alt="images">
                                        <img class="secondary_image" src="../App_Themes/products/assets/images/products/2.jpg" alt="images">
                                    </a>
                                    <p class="description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                                    <div class="action">
                                        <a class="refresh" href="#"><i class="zmdi zmdi-refresh-sync"></i></a>
                                        <a href="#" title="Like"><i class="zmdi zmdi-favorite-outline"></i></a>
                                        <a href="#" title="add-to-cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="item-inner">
                                <div class="product ">
                                    <span class="new lable">New</span>
                                    <p class="product-title">Apple watch sport green</p>
                                    <p class="product-price"><span>price: </span>$ 650.99</p>
                                    <a class="product-images" href="#" title="">
                                        <img class="primary_image" src="../App_Themes/products/assets/images/products/8.jpg" alt="images">
                                        <img class="secondary_image" src="../App_Themes/products/assets/images/products/4.jpg" alt="images">
                                    </a>
                                    <p class="description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                                    <div class="action">
                                        <a class="refresh" href="#"><i class="zmdi zmdi-refresh-sync"></i></a>
                                        <a href="#" title="Like"><i class="zmdi zmdi-favorite-outline"></i></a>
                                        <a href="#" title="add-to-cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="item-inner">
                                <div class="product">
                                    <p class="product-title">Apple watch sport green</p>
                                    <p class="product-price"><span>price: </span>$ 650.99</p>
                                    <a class="product-images" href="#" title="">
                                        <img class="primary_image" src="../App_Themes/products/assets/images/products/9.jpg" alt="images">
                                        <img class="secondary_image" src="../App_Themes/products/assets/images/products/6.jpg" alt="images">
                                    </a>
                                    <p class="description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                                    <div class="action">
                                        <a class="refresh" href="#"><i class="zmdi zmdi-refresh-sync"></i></a>
                                        <a href="#" title="Like"><i class="zmdi zmdi-favorite-outline"></i></a>
                                        <a href="#" title="add-to-cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="item-inner">
                                <div class="product">
                                    <p class="product-title">Apple watch sport green</p>
                                    <p class="product-price"><span>price: </span>$ 650.99</p>
                                    <a class="product-images" href="#" title="">
                                        <img class="primary_image" src="../App_Themes/products/assets/images/products/10.jpg" alt="images">
                                        <img class="secondary_image" src="../App_Themes/products/assets/images/products/5.jpg" alt="images">
                                    </a>
                                    <p class="description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                                    <div class="action">
                                        <a class="refresh" href="#"><i class="zmdi zmdi-refresh-sync"></i></a>
                                        <a href="#" title="Like"><i class="zmdi zmdi-favorite-outline"></i></a>
                                        <a href="#" title="add-to-cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="item-inner">
                                <div class="product sale">
                                    <span class="sale lable">Sale</span>
                                    <p class="product-title">Apple watch sport green</p>
                                    <div class="product-price">
                                        <span>price: </span><span class="price">$ 650.99</span>
                                        <span class="price-old">$ 670.99</span>
                                    </div>
                                    <a class="product-images" href="#" title="">
                                        <img class="primary_image" src="../App_Themes/products/assets/images/products/11.jpg" alt="images">
                                        <img class="secondary_image" src="../App_Themes/products/assets/images/products/3.jpg" alt="images">
                                    </a>
                                    <p class="description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                                    <div class="action">
                                        <a class="refresh" href="#"><i class="zmdi zmdi-refresh-sync"></i></a>
                                        <a href="#" title="Like"><i class="zmdi zmdi-favorite-outline"></i></a>
                                        <a href="#" title="add-to-cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="item-inner">
                                <div class="product ">
                                    <span class="new lable">New</span>
                                    <p class="product-title">Apple watch sport green</p>
                                    <p class="product-price"><span>price: </span>$ 650.99</p>
                                    <a class="product-images" href="#" title="">
                                        <img class="primary_image" src="../App_Themes/products/assets/images/products/12.jpg" alt="images">
                                        <img class="secondary_image" src="../App_Themes/products/assets/images/products/1.jpg" alt="images">
                                    </a>
                                    <p class="description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                                    <div class="action">
                                        <a class="refresh" href="#"><i class="zmdi zmdi-refresh-sync"></i></a>
                                        <a href="#" title="Like"><i class="zmdi zmdi-favorite-outline"></i></a>
                                        <a href="#" title="add-to-cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <!-- End product-content products  -->
                    <div class="pagination-container">
                        <nav class="pagination">    
                            <span class="page-numbers current">1</span>
                            <a class="page-numbers" href="#">2</a>
                            <a class="page-numbers" href="#">3</a>
                            ...
                            <a class="page-numbers" href="#">8</a>
                        </nav>
                    </div>
                    <!-- End pagination-container -->
            </div>
            <!-- End Primary -->
            <div id="secondary" class="col-lg-3 col-md-3 col-sm-12 col-xs-12 visible-sm visible-xs">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_rightside_job_categories_wrapper">
                                <div class="jp_rightside_job_categories_heading">
                                    <h4>Jobs by Category</h4>
                                </div>
                                <div class="jp_rightside_job_categories_content">
                                    <div class="handyman_sec1_wrapper">
                                        <div class="content">
                                            <div class="box">
                                                <p>
                                                    <input type="checkbox" id="c101" name="cb">
                                                    <label for="c101">Graphic Designer <span>(214)</span></label>

                                                    <p>
                                                        <input type="checkbox" id="c102" name="cb">
                                                        <label for="c102">Engineering Jobs <span>(514)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c103" name="cb">
                                                        <label for="c103">Mainframe Jobs <span>(554)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c104" name="cb">
                                                        <label for="c104">Legal Jobs <span>(457)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c105" name="cb">
                                                        <label for="c105">IT Jobs <span>(1254)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c106" name="cb">
                                                        <label for="c106">R&D Jobs <span>(554)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c107" name="cb">
                                                        <label for="c107">Government Jobs <span>(350)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c108" name="cb">
                                                        <label for="c108">PSU Jobs <span>(221)</span></label>
                                                    </p>
                                            </div>
                                        </div>
                                        <ul>
                                            <li><i class="fa fa-plus-circle"></i> <a href="#">SHOW MORE</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_rightside_job_categories_wrapper jp_job_location_wrapper">
                                <div class="jp_rightside_job_categories_heading">
                                    <h4>Jobs by Location</h4>
                                </div>
                                <div class="jp_rightside_job_categories_content">
                                    <div class="handyman_sec1_wrapper">
                                        <div class="content">
                                            <div class="box">
                                                <p>
                                                    <input type="checkbox" id="c109" name="cb">
                                                    <label for="c109">Jobs in Delhi  <span>(214)</span></label>

                                                    <p>
                                                        <input type="checkbox" id="c110" name="cb">
                                                        <label for="c110">Jobs in Mumbai <span>(514)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c111" name="cb">
                                                        <label for="c111">Jobs in Chennai <span>(554)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c112" name="cb">
                                                        <label for="c112">Jobs in Gurgaon <span>(457)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c113" name="cb">
                                                        <label for="c113">Jobs in Noida  <span>(1254)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c114" name="cb">
                                                        <label for="c114">Jobs in Kolkata <span>(554)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c115" name="cb">
                                                        <label for="c115">Jobs in Hyderabad <span>(350)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c116" name="cb">
                                                        <label for="c116">Jobs in Pune <span>(221)</span></label>
                                                    </p>
                                            </div>
                                        </div>
                                        <ul>
                                            <li><i class="fa fa-plus-circle"></i> <a href="#">SHOW MORE</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_rightside_job_categories_wrapper jp_job_location_wrapper">
                                <div class="jp_rightside_job_categories_heading">
                                    <h4>Your Skill’s</h4>
                                </div>
                                <div class="jp_rightside_job_categories_content">
                                    <div class="handyman_sec1_wrapper">
                                        <div class="content">
                                            <div class="box">
                                                <p>
                                                    <input type="checkbox" id="c117" name="cb">
                                                    <label for="c117">Javascript  <span>(214)</span></label>

                                                    <p>
                                                        <input type="checkbox" id="c118" name="cb">
                                                        <label for="c118">HTML5 <span>(514)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c119" name="cb">
                                                        <label for="c119">CSS3 <span>(554)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c120" name="cb">
                                                        <label for="c120">PHP  <span>(457)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c121" name="cb">
                                                        <label for="c121">Sales   <span>(1254)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c122" name="cb">
                                                        <label for="c122">Marketing  <span>(554)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c123" name="cb">
                                                        <label for="c123">Social Media <span>(350)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c124" name="cb">
                                                        <label for="c124">Web Design <span>(221)</span></label>
                                                    </p>
                                            </div>
                                        </div>
                                        <ul>
                                            <li><i class="fa fa-plus-circle"></i> <a href="#">SHOW MORE</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_rightside_job_categories_wrapper jp_job_location_wrapper">
                                <div class="jp_rightside_job_categories_heading">
                                    <h4>Salary</h4>
                                </div>
                                <div class="jp_rightside_job_categories_content">
                                    <div class="handyman_sec1_wrapper">
                                        <div class="content">
                                            <div class="box">
                                                <p>
                                                    <input type="checkbox" id="c125" name="cb">
                                                    <label for="c125">$1k - 2k  <span>(214)</span></label>

                                                    <p>
                                                        <input type="checkbox" id="c126" name="cb">
                                                        <label for="c126">$3k - 5k <span>(514)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c127" name="cb">
                                                        <label for="c127">$5k - 8k <span>(554)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c128" name="cb">
                                                        <label for="c128">$5k - 10k  <span>(457)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c129" name="cb">
                                                        <label for="c129">$10k - 20k   <span>(1254)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c130" name="cb">
                                                        <label for="c130">$20k - 30k  <span>(554)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c131" name="cb">
                                                        <label for="c131">$30k - 50k <span>(350)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c132" name="cb">
                                                        <label for="c132">$50k - 80k <span>(221)</span></label>
                                                    </p>
                                            </div>
                                        </div>
                                        <ul>
                                            <li><i class="fa fa-plus-circle"></i> <a href="#">SHOW MORE</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_rightside_job_categories_wrapper jp_job_location_wrapper">
                                <div class="jp_rightside_job_categories_heading">
                                    <h4>Types</h4>
                                </div>
                                <div class="jp_rightside_job_categories_content">
                                    <div class="handyman_sec1_wrapper">
                                        <div class="content">
                                            <div class="box">
                                                <p>
                                                    <input type="checkbox" id="c133" name="cb">
                                                    <label for="c133">Types  <span>(214)</span></label>

                                                    <p>
                                                        <input type="checkbox" id="c134" name="cb">
                                                        <label for="c134">Part-time <span>(514)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c135" name="cb">
                                                        <label for="c135">Contract <span>(554)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c136" name="cb">
                                                        <label for="c136">Remotely  <span>(457)</span></label>
                                                    </p>
                                                    <p>
                                                        <input type="checkbox" id="c137" name="cb">
                                                        <label for="c137">Temporary   <span>(1254)</span></label>
                                                    </p>
                                            </div>
                                        </div>
                                        <ul>
                                            <li><i class="fa fa-plus-circle"></i> <a href="#">SHOW MORE</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="jp_add_resume_wrapper jp_job_location_wrapper">
                                <div class="jp_add_resume_img_overlay"></div>
                                <div class="jp_add_resume_cont">
                                    <img src="../jobpro/images/content/resume_logo.png" alt="logo" />
                                    <h4>Get Best Matched Jobs On your Email. Add Resume NOW!</h4>
                                    <ul>
                                        <li><a href="#"><i class="fa fa-plus-circle"></i> &nbsp;ADD RESUME</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 visible-sm visible-xs">
                            <div class="pager_wrapper gc_blog_pagination">
                                <ul class="pagination">
                                    <li><a href="#">Priv.</a></li>
                                    <li><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li class="hidden-xs"><a href="#">4</a></li>
                                    <li><a href="#">Next</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
        </div>           
        <!-- End conainer -->
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cpScript" Runat="Server">
    <script type="text/javascript" src="../App_Themes/products/assets/js/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="../App_Themes/products/assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../App_Themes/products/assets/js/owl.carousel.min.js"></script>
        <script type="text/javascript" src="../App_Themes/products/assets/js/price-range.js"></script>
        <script type="text/javascript" src="../App_Themes/products/assets/js/wow.min.js"></script>
        <script type="text/javascript">
          jQuery("#Slider1").slider({ 
            from: 0,
            to: 1000,
            step: 100,
            smooth: true,
            round: 0,
            skin: "plastic"
        });
        </script>
        <script type="text/javascript" src="../App_Themes/products/assets/js/store.js"></script>
</asp:Content>

