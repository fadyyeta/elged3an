﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;


/// <summary>
/// Summary description for EmergencyClass
/// </summary>
public class EmergencyClass
{
    public static DataSet ShowAllEmergencies()
    {
        DataSet ds;
        try
        {
            using (SqlConnection con = connectionManager.getconnection())
            {
                ds = new DataSet();
                SqlDataAdapter adpter = new SqlDataAdapter("selectAllEmergencies", con);
                adpter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adpter.Fill(ds);
            }
            if (ds == null || ds.Tables[0] == null || ds.Tables[0].Rows.Count == 0)
            {
                return null;
            }
            else
            {
                return ds;
            }
        }
        catch (Exception)
        {

            return null;
        }

    }
}